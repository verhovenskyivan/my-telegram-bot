from aiogram.types import Message, ReplyKeyboardMarkup, KeyboardButton

# --- Buttons ---
btnRandom = KeyboardButton('Случайное число')
btnSchedule = KeyboardButton('Расписание')
btnInfo = KeyboardButton('Инфо')
Menu = ReplyKeyboardMarkup(resize_keyboard = True).add(btnRandom, btnSchedule, btnInfo)