from aiogram import Bot, Dispatcher, executor, types
import aiohttp, asyncio, logging
import Buttons as Navigation
import random
import api_work as api
import requests
from bs4 import BeautifulSoup

# --- Tokens and Bot ---
TOKEN = ''
My_Bot = Bot(token=TOKEN)
MB_Dp = Dispatcher(My_Bot)


# --- Functions ---
@MB_Dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    await My_Bot.send_message(message.from_user.id, 'Привет {0.first_name}'.format(message.from_user), reply_markup = Navigation.Menu)
    
@MB_Dp.message_handler(commands=['help'])
async def bot_message(message: types.Message):
    await My_Bot.send_message(message.from_user.id, 'Чем тебе помочь?'.format(message.from_user))
    
@MB_Dp.message_handler()
async def handler(message: types.Message):
    
    if message.text == 'Случайное число':
        await My_Bot.send_message(message.from_user.id, 'Твое число: ' +str(random.randint(0, 9999)))        
    
    elif message.text == 'Расписание':
        await My_Bot.send_message(message.from_user.id, 'Введите дату в формате ДД.ММ.ГГ: ')
        url = 'https://www.tutu.ru/rasp.php?st1=8302&st2=9802'
        query = (message.from_user.id, message.text)
        src = f'{url}+&date={query}'    
        content = requests.get(src).text
        soup = BeautifulSoup(content, 'html.parser')
        for element in soup.find_all('a', class_ = 'g-link desktop__depTimeLink__1NA_N'):
            message.reply(str(element.text))        
    else:
        await message.reply('Неизвестная команда')        
        
@MB_Dp.message_handler(commands='schedule')
async def schedule(message: types.Message):
    url = 'https://www.tutu.ru/rasp.php?st1=8302&st2=9802'
    query = My_Bot.send_message('Введите дату в формате ДД.ММ.ГГ: ')
    src = f'{url}+&date={query}'
    content = requests.get(src).text
    soup = BeautifulSoup(content, 'html.parser')
    for element in soup.find_all('a', class_ = 'g-link desktop__depTimeLink__1NA_N'):
        message.replY(str(element.text))
    await My_Bot.send_message(message.from_user.id, schedule(), reply_markup = Navigation.btnSchedule)        
        
# -- Bot poilling ---        
if __name__ == '__main__':
    executor.start_polling(MB_Dp, skip_updates = True)
