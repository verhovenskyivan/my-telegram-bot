from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
import requests


s = Service('./workscript-main/chromedriver.exe')

options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)
#options.add_argument("--headless")

driver = webdriver.Chrome(options = options, service=Service(ChromeDriverManager().install()))


driver.get('https://www.tutu.ru/prigorod/')

From_station = input('Откуда: ')
To_station = input('Куда: ')
query = input('Когда: ')

#def get_schedule()

def choose_day(query):
    if query == 'Сегодня' or 'сегодня':
        driver.find_element(By.XPATH, '//*[@id="wrapper"]/div[2]/div/form/div/div/div[5]/button/span/span[3]').click()
    elif query == 'Завтра' or 'завтра':
        driver.find_element(By.XPATH, '//*[@id="wrapper"]/div[2]/div/form/div/div/div[4]/div/div[1]/button[2]').send_keys(Keys.ENTER)
        driver.find_element(By.XPATH, '//*[@id="wrapper"]/div[2]/div/form/div/div/div[5]/button/span/span[3]').send_keys(Keys.ENTER)

driver.find_element(By.NAME, 'st1').send_keys(From_station)
driver.find_element(By.NAME, 'st2').send_keys(To_station)
choose_day(query)
#driver.page_source
first_shedule = driver.find_element(By.XPATH, '//*[@id="timetable"]/div[1]/div[1]/div/div/table/tbody/tr[29]/td[1]/a').text 
second_schedule = driver.find_element(By.XPATH, '//*[@id="timetable"]/div[1]/div[1]/div/div/table/tbody/tr[32]/td[1]/a').text
third_schedule = driver.find_element(By.XPATH, '//*[@id="timetable"]/div/div[1]/div/div/table/tbody/tr[31]/td[1]').text
print('Ближайшее время: ' + first_shedule + ', ' + second_schedule + ', ' + third_schedule)