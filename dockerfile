FROM python:3.10-alpine

WORKDIR /bot

COPY . .

RUN  pip install --upgrade pip && pip install aiogram requests beautifulsoup4

CMD ["python", "Aio.py"] 